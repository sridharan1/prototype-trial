import { Injectable, NotFoundException } from '@nestjs/common';

@Injectable()
export class AppService {
  getHello(): string {
    return 'Hello World!';
  }

  pingError(): string {
    throw new NotFoundException({ message: "ping failed" });
  }

  ping(): string {
    return 'pong!';
  }
}
