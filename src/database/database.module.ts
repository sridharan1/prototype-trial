import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule, ConfigService } from '@nestjs/config';
import * as path from 'path';

@Module({
  imports: [
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => ({
        type: 'postgres',
        synchronize: true,
        url: configService.get('DATABASE_URL'),
        // ssl: {
        //   rejectUnauthorized: false,
        // },
        entities: [path.resolve(__dirname, './../**/*.entity{.ts,.js}')],
      }),
    }),
  ],
})
export class DatabaseModule {}
