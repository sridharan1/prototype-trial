import { PartialType } from '@nestjs/mapped-types';
import { CreateFactoryDto } from './create-factory.dto';
import { ApiProperty } from '@nestjs/swagger';
import { ChainId } from '../factory.enum';

export class UpdateFactoryDto extends PartialType(CreateFactoryDto) {
  @ApiProperty({ type: String, required: true })
  address: string;

  @ApiProperty({
    type: 'enum',
    enum: ChainId,
    default: ChainId.ETH_GOERLI,
    required: true,
  })
  chainId: ChainId;

  @ApiProperty({ type: Number, required: true })
  lastPolledBlock: number;
}
