import { ApiProperty } from '@nestjs/swagger';
import { ChainId } from '../factory.enum';

export class CreateFactoryDto {
  @ApiProperty({ type: String, required: true })
  address: string;

  @ApiProperty({
    type: 'enum',
    enum: ChainId,
    default: ChainId.ETH_GOERLI,
    required: true,
  })
  chainId: ChainId;

  @ApiProperty({ type: Number, required: true })
  lastPolledBlock: number;
}