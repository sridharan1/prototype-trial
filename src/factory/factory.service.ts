import { Injectable } from '@nestjs/common';
import { CreateFactoryDto } from './dto/create-factory.dto';
import { UpdateFactoryDto } from './dto/update-factory.dto';
import { Factory } from './entities/factory.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class FactoryService {
  constructor(
    @InjectRepository(Factory)
    private readonly factoryRepository: Repository<Factory>,
  ) {}
  create(createFactoryDto: CreateFactoryDto) {
    this.factoryRepository.save(createFactoryDto);
    return {message: "Factory record created"};
  }

  findAll() {
    return this.factoryRepository.find({});;
  }

  update(id: number, updateFactoryDto: UpdateFactoryDto) {
    const updateFactoryRecord =  { id: id, ...updateFactoryDto};
    this.factoryRepository.save(updateFactoryRecord);
    return `Factory record - ${id} - has been updated`;
  }
}
