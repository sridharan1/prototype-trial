export enum ChainId {
  ETH = '0x1',
  POLYGON = '0x89',
  BSC = '0x38',
  ETH_GOERLI = '0x5',
  POLYGON_MUMBAI = '0x13881',
  BSC_TESTNET = '0x61',
}
