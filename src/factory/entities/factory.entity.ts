import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { ChainId } from '../factory.enum';

@Entity()
export class Factory {
  @PrimaryGeneratedColumn()
  public id: number;

  @Column({ nullable: false })
  public address: string;

  @Column({ type: 'enum', enum: ChainId, default: ChainId.ETH_GOERLI })
  public chainId: ChainId;

  @Column({ nullable: true })
  public lastPolledBlock: number;
}
